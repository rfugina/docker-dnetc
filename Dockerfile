
FROM ubuntu:bionic

RUN apt-get update && apt-get -y upgrade && apt-get install -y wget && apt-get -y autoremove

RUN mkdir -m 0777 /var/dnetc
WORKDIR /var/dnetc

# get software
RUN wget -O - --no-verbose ftp://ftp.distributed.net/dcti/v2.9112/dnetc521-linux-amd64-uclibc.tar.gz | tar zxvf - --strip-components=1

# import my config and scripts
ADD dnetc.ini run_one_block test_script.sh /var/dnetc/

ENV PATH /var/dnetc:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

CMD /var/dnetc/dnetc

# run with: docker run -it dnetc
# run in background: docker run -d dnetc
# run one work unit: docker run -it --rm dnetc /var/dnetc/run_one_block
# run specific number of work units: docker run -it --rm dnetc /var/dnetc/run_one_block X

