
default:: image

image::
	docker build -t dnetc .

push:: image
	docker tag dnetc rfugina/dnetc
	docker push rfugina/dnetc

pull::
	docker pull registry.gitlab.com/rfugina/docker-dnetc

shell:: image
	docker run -it --rm dnetc bash

run:: image
	docker run -it --rm dnetc /var/dnetc/run_one_block

